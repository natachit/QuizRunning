package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.TimeUtils;
public class World {
    static Sound endSound;
    public Sound nextStageSound;
    private Me me1;
    private Me me2;
    private QuizRunning quizRunning;
    private Map map;
    private WorldRenderer worldRenderer;
    private int[] meStatus = {0,0};
    private static int gameStatus = 0;
    private float stillTime = 2;
    private int winnerPoint;
    private static int stage = 1;
    private static final int STAGE_MAX = 12;
    public float realTime = 30;
    public int time;
    
    World(QuizRunning quizRunning) {
        map = new Map();
        me1 = new Me(quizRunning.START_POSITION[0][0], quizRunning.START_POSITION[0][1], this);
        me2 = new Me (quizRunning.START_POSITION[1][0], quizRunning.START_POSITION[1][1], this);
        this.quizRunning = quizRunning;
        endSound = Gdx.audio.newSound(Gdx.files.internal("sound/end.wav"));
        nextStageSound = Gdx.audio.newSound(Gdx.files.internal("sound/next.wav"));
    }
 
    Me getMe(int x){
        if (x == 1) {
            return me1;
        }
        else { 
            return me2;
        }
    }
    
    Map getMap() {
        return map;
    }
    
    public void update(float delta) {
        updateMeStatus();
        if (me1.getStatus() == 1) {
            stillTime -= delta;
            me1.update();
            if (stillTime <= 0) {
                me1.resetStatus();
                stillTime = 2;
            }
        }
        else if (me2.getStatus() == 1) {
            stillTime -= delta;
            me2.update();
            if (stillTime <= 0) {
                me2.resetStatus();
                stillTime = 2;
            }
        }
        else {
            me1.update();
            me2.update();
        }
    }
    
    public void calculateTime(float delta) {
        if (gameStatus == 1) {
            realTime -= delta;
        }
        else {}
        time = (int)realTime;
        if (realTime <= 0) {
            gameStatus = 2;
        }
    }
    
    public int getTime() {
        return time;
    }
    
    public static int getStage() {
        return stage;
    }
    
    public static void addStage() {
        stage++;
        checkFinalStage();
    }
    
    public void resetPosition() {
        me1.position.x = quizRunning.START_POSITION[0][0];
        me1.position.y = quizRunning.START_POSITION[0][1];
        me2.position.x = quizRunning.START_POSITION[1][0];
        me2.position.y = quizRunning.START_POSITION[1][1];
    }
    
    public static int getGameStatus() {
        return gameStatus;
    }
    
    public void changeGameStatus(int x) {
        gameStatus = x;
        realTime = 30;
    }
    
    private void updateMeStatus() {
        meStatus[0] = me2.getStatus();
        meStatus[1] = me1.getStatus();
    }
    
    public int getMeStatus(int x) {
        return meStatus[x];
    }
    
    public String getWinner() {
        if (me1.getPoint() > me2.getPoint()) {
            winnerPoint = me1.getPoint();
            return "The winner is player 2";
        }
        else if (me1.getPoint() < me2.getPoint()) {
            winnerPoint = me2.getPoint();
            return "The winner is player 1";
        }
        else {
            winnerPoint = me1.getPoint();
            return "     It was a draw";
        }
    }
    
    public int getWinnerPoint() {
        return winnerPoint;
    }
    
    private static void checkFinalStage() {
        if (stage == STAGE_MAX + 1) {
            gameStatus = 4;
            GameScreen.themeSound.dispose();
            endSound.play();
        }
    }
}

