package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;

public class Me {
    Sound rightSound;
    Sound wrongSound;
    Sound starSound;
    private int currentDirection;
    private int nextDirection;
    private Map map;
    private World world;
    private int point = 0;
    private int status = 0;
    public Vector2 position;
    public static final int DIRECTION_UP = 1;
    public static final int DIRECTION_RIGHT = 2;
    public static final int DIRECTION_DOWN = 3;
    public static final int DIRECTION_LEFT = 4;
    public static final int DIRECTION_STILL = 0;
    public static final int SPEED = 5;
    private static final int [][] DIR_OFFSETS = new int [][] {
        {0,0},
        {0,-1},
        {1,0},
        {0,1},
        {-1,0}
    };
 
    public Me(int x, int y, World world) {
        position = new Vector2(x, y);
        currentDirection = DIRECTION_STILL;
        nextDirection = DIRECTION_STILL;
        this.world = world;
        rightSound = Gdx.audio.newSound(Gdx.files.internal("sound/right.wav"));
        wrongSound = Gdx.audio.newSound(Gdx.files.internal("sound/wrong.wav"));
        starSound = Gdx.audio.newSound(Gdx.files.internal("sound/getstar.wav"));
    }    
 
    public Vector2 getPosition() {
        return position;    
    }
    
    public void move(int dir) { 
        position.x += SPEED * DIR_OFFSETS[dir][0];
        position.y += SPEED * DIR_OFFSETS[dir][1];
    }
    
    public void setNextDirection(int dir) {
        nextDirection = dir;
    }
    
    public void update() {
        Map map = world.getMap();
        if(isAtCenter()) {
            if(canMoveInDirection(nextDirection)) {
                currentDirection = nextDirection;
                if (map.hasStarAt(getRow(), getColumn())) {
                    map.removeStarAt(getRow(), getColumn());
                    status = 1;
                    point += 10;
                    starSound.play();
                }
            } else {
                currentDirection = DIRECTION_STILL;    
            }
        }
        position.x += SPEED * DIR_OFFSETS[currentDirection][0];
        position.y += SPEED * DIR_OFFSETS[currentDirection][1];
        checkAns();
    }
    
    public boolean isAtCenter() {
        int blockSize = WorldRenderer.BLOCK_SIZE;
 
        return ((((int)position.x - blockSize / 2) % blockSize) == 0) &&
                ((((int)position.y - blockSize / 2) % blockSize) == 0);
    }
     
    private boolean canMoveInDirection(int dir) {
        Map map = world.getMap();
        int newRow = getRow() + DIR_OFFSETS[dir][1];
        int newCol = getColumn() + DIR_OFFSETS[dir][0];
        return !(map.hasWallAt(newRow,newCol) != 0 || map.hasBushAt(newRow, newCol) );
    }
    
    private int getRow() {
        return ((int)position.y) / WorldRenderer.BLOCK_SIZE; 
    }
 
    private int getColumn() {
        return ((int)position.x) / WorldRenderer.BLOCK_SIZE; 
    }
    
    private void checkAns() {
        int y = getRow();
        int x = getColumn();
        String ans = "";
        if (y == 4 || y == 5) {
            if (x == 1 || x == 2) {
                ans = "1";
                updateLevel(ans);
            }
            else if (x == 17 || x == 16) {
                ans = "2";
                updateLevel(ans);
            }
        }
    }
    private void updateLevel(String ans) {
        if (ans == map.getQA(3) || map.getQA(3) == "0") {
            World.addStage();
            point += world.time;
            rightSound.play(0.5f);
        }
        else {
            point -= 20;
            world.changeGameStatus(3);
            wrongSound.play(0.8f);
        }
        status = 0;
        Map map = world.getMap();
        map.randomMap();
        world.resetPosition();
        world.realTime = 30;
    }
    
    public int getPoint() {
        return point;
    }
    
    public int getStatus() {
        return status;
    }
    
    public void resetStatus() {
        status = 0;
    }
}
