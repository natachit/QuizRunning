package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.math.Vector2;

public class WorldRenderer {
    private QuizRunning quizRunning;
    private World world;
    private Texture page0Img;
    private Texture page1Img;
    private Texture page2Img;
    private Texture page4Img;
    private Texture frameImg;
    private Me me1;
    private Me me2;
    private MapRenderer mapRenderer;
    private BitmapFont font;
    private BitmapFont fontBig;
    private BitmapFont fontMid;
    private BitmapFont fontMe;
    private BitmapFont fontMeStill;
    private Map map;
    public SpriteBatch batch;
    public static final int BLOCK_SIZE = 40;
    FreeTypeFontGenerator generator;
    FreeTypeFontGenerator generatorMe;
    FreeTypeFontParameter parameter;
    FreeTypeFontParameter parameterBig;
    FreeTypeFontParameter parameterMid;
    FreeTypeFontParameter parameterMe;
    FreeTypeFontParameter parameterMeStill;
    
    public WorldRenderer(QuizRunning quizRunning, World world) {
        this.quizRunning = quizRunning;
        this.world = world;
        frameImg = new Texture("frame.png");
        mapRenderer = new MapRenderer(quizRunning.batch, world.getMap());
        me1 = world.getMe(1);
        me2 = world.getMe(2);
        page0Img = new Texture("page0.png");
        page1Img = new Texture("page1.png");
        page2Img = new Texture("page2.png");
        page4Img = new Texture("page4.png");
        batch = quizRunning.batch;
        generator = new FreeTypeFontGenerator(Gdx.files.internal("pixelmix.ttf"));
        parameter = new FreeTypeFontParameter();
        parameterBig = new FreeTypeFontParameter();
        parameterMid = new FreeTypeFontParameter();
        parameter.size = 15;
        parameterBig.size = 50;
        parameterMid.size = 35;
        font = generator.generateFont(parameter);
        fontBig = generator.generateFont(parameterBig);
        fontMid = generator.generateFont(parameterMid);
        generator.dispose();
        generatorMe = new FreeTypeFontGenerator(Gdx.files.internal("INVADERS.TTF"));
        parameterMe = new FreeTypeFontParameter();
        parameterMe.size = 25;
        parameterMe.color = Color.WHITE;
        fontMe = generatorMe.generateFont(parameterMe);
        parameterMeStill = new FreeTypeFontParameter();
        parameterMeStill.size = 25;
        parameterMeStill.color = Color.RED;
        fontMeStill = generatorMe.generateFont(parameterMeStill);
        generatorMe.dispose();
    }
    
    public void render(float delta) {
        mapRenderer.render();
        Vector2 pos1 = world.getMe(1).getPosition();
        Vector2 pos2 = world.getMe(2).getPosition();
        batch.begin();
        batch.draw(frameImg, 0, 0);
        if (world.getMeStatus(0) == 0 && world.getMeStatus(1) == 1) {
            fontMe.draw(batch, "D", pos1.x - BLOCK_SIZE / 2 + 3, 
                        QuizRunning.HEIGHT - pos1.y - BLOCK_SIZE / 2 + 28);
            fontMeStill.draw(batch, "@", pos2.x - BLOCK_SIZE / 2 + 10, 
                        QuizRunning.HEIGHT - pos2.y - BLOCK_SIZE / 2 + 30);
        }
        else if (world.getMeStatus(0) == 1 && world.getMeStatus(1) == 0) {
            fontMeStill.draw(batch, "D", pos1.x - BLOCK_SIZE / 2 + 3, 
                        QuizRunning.HEIGHT - pos1.y - BLOCK_SIZE / 2 + 28);
            fontMe.draw(batch, "@", pos2.x - BLOCK_SIZE / 2 + 10, 
                        QuizRunning.HEIGHT - pos2.y - BLOCK_SIZE / 2 + 30);
        }
        else {
            fontMe.draw(batch, "D", pos1.x - BLOCK_SIZE / 2 + 3, 
                        QuizRunning.HEIGHT - pos1.y - BLOCK_SIZE / 2 + 28);
            fontMe.draw(batch, "@", pos2.x - BLOCK_SIZE / 2 + 10, 
                        QuizRunning.HEIGHT - pos2.y - BLOCK_SIZE / 2 + 30);
        }
        switch (world.getGameStatus()) {
            case 0 : 
                batch.draw(page0Img, 0, 40);
                fontBig.draw(batch, "QUIZ RUNNING", 180, 640);
                font.draw(batch, "Press ENTER start the game", 400, 100);
                break;
            case 1 : 
                batch.draw(frameImg, 0, 0);
                font.draw(batch, "" + world.getTime(), 700, 640);
                font.draw(batch, "STAGE " + World.getStage(), 600, 640);
                font.draw(batch, "point " + me2.getPoint(), 20, 30);
                font.draw(batch, " point " + me1.getPoint(), 650, 30);
                font.draw(batch, "" + Map.getQA(0), 100, 640);
                font.draw(batch, "" + Map.getQA(1), 70, 490);
                font.draw(batch, "" + Map.getQA(2), 670, 490);
                font.draw(batch, "Press Space Bar to random new map", 200, 30);
                break;
            case 2 : 
                batch.draw(page2Img, 200, 200); 
                fontBig.draw(batch, "Time's Up!!", 220, 320);
                font.draw(batch, "Press ENTER to try again", 260, 230);
                break;
            case 3 :
                batch.draw(page2Img, 200, 200); 
                fontMid.draw(batch, "WRONG ANSWER!!", 220, 320);
                font.draw(batch, "Press ENTER to try again", 260, 230);
                break;
            case 4 :
                batch.draw(page4Img, 0, 40); 
                fontBig.draw(batch, "QUIZ RUNNING", 180, 640);
                fontMid.draw(batch, "STAGE COMPLETE!!", 200, 480);
                fontMid.draw(batch, world.getWinner(), 130, 330);
                fontMid.draw(batch, "You got " + world.getWinnerPoint() + " points", 180, 180);
                break;
            default : 
                break;
        }
        batch.end();

    }
}

