package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class MapRenderer {
    private Map map;
    private SpriteBatch batch;
    private Texture bushImage;
    private Texture starImage;
    
    public MapRenderer(SpriteBatch batch, Map map) {
        this.map = map;
        this.batch = batch;
    }
 
    public void render() {
        bushImage = new Texture("bush.png");
        starImage = new Texture("star.png");
        batch.begin();
        for(int r = 0; r < map.getHeight(); r++) {
            for(int c = 0; c < map.getWidth(); c++) {
                int x = c * WorldRenderer.BLOCK_SIZE;
                int y = QuizRunning.HEIGHT - 
                        (r * WorldRenderer.BLOCK_SIZE) - WorldRenderer.BLOCK_SIZE;
                if(map.hasBushAt(r, c)) {
                    batch.draw(bushImage, x, y);
                } 
                else if(map.hasStarAt(r, c)) {
                    batch.draw(starImage, x, y);
                }
            }
        }
        batch.end();
    }
}

