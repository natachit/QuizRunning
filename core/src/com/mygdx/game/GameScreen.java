package com.mygdx.game;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.QuizRunning;

public class GameScreen extends ScreenAdapter {
    private QuizRunning quizRunning;
    private Texture meImg;
    private Me me1;
    private Me me2;
    private Map map;
    private WorldRenderer worldRenderer;
    private World world;
    private int count = 0;
    public static Sound themeSound;
    
    public GameScreen(QuizRunning quizRunning) {
        this.quizRunning = quizRunning;
        world = new World(quizRunning);
        me1 = world.getMe(1);
        me2 = world.getMe(2);
        map = world.getMap();
        worldRenderer = new WorldRenderer(quizRunning, world);
        themeSound = Gdx.audio.newSound(Gdx.files.internal("sound/theme song.wav"));
        themeSound.loop();
    }
    
    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor((float)160/255, (float)149/255, (float)122/255, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        worldRenderer.render(delta);
        world.calculateTime(delta);
    }
    
    private void update(float delta) {
        updateMeDirection();
        world.update(delta);
    }
    
    private void updateMeDirection() {
        if (world.getGameStatus() != 1) {
            if (Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
                world.changeGameStatus(1);
                world.nextStageSound.play();
            }
            else {
                me1.setNextDirection(Me.DIRECTION_STILL);
                me2.setNextDirection(Me.DIRECTION_STILL);
            }
        }
        
        else if (world.getGameStatus() == 1) {
            if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
                map.userRandomMap(world.getStage());
            }
            
            if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
                me1.setNextDirection(Me.DIRECTION_UP);
            }
            else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                me1.setNextDirection(Me.DIRECTION_DOWN);
            }
            else if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                me1.setNextDirection(Me.DIRECTION_LEFT);
            }
            else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                me1.setNextDirection(Me.DIRECTION_RIGHT);
            }
            else {
                me1.setNextDirection(Me.DIRECTION_STILL);
            }
        
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                me2.setNextDirection(Me.DIRECTION_UP);
            }
            else if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                me2.setNextDirection(Me.DIRECTION_DOWN);
            }
            else if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                me2.setNextDirection(Me.DIRECTION_LEFT);
            }
            else if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                me2.setNextDirection(Me.DIRECTION_RIGHT);
            }
            else {
                me2.setNextDirection(Me.DIRECTION_STILL);
            }
        }
    }
}
