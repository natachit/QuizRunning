package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.NumberUtils;
import java.util.Random;

public class Map {
    private final String[] MAP_ORIGINAL = new String [] {
             "-------------------",
             "-------------------",
             "-------------------",
             "7888888888888888889",
             "4000###########0006",
             "4000###########0006",
             "4#################6",
             "4#################6",
             "4#################6",
             "4#################6",
             "4#################6",
             "4#################6",
             "4#################6",
             "4#################6",
             "4#################6",
             "1222222222222222223",  
             "-------------------"
    };
    public static final String[][] QUESTION = {
            {"2 + 7 * 2 - 6 / 3 + 3 = ?","17","18","1","0"},
            {"How many days are in 2015?","365","366","1","0"},
            {"1+1","1","2","2","0"},
            {"2+2","4","8","1","0"},
            {"2+2*2","6","8","1","0"},
            {"\"aswedfrcvbxnxbvcfdewsa\" is a palindrome.","true","false","2","0"},
            {"Mameaw was born on Oct __, 1996.","13","14","2","0"},
            {"Tawan was born on __, 18, 1997.","April","May","2","0"},
            {"Eye was born on Jan, 11, __.","1996","1997","2","0"},
            {"Left or Right ?","Left","Right","1","0"},
            {"\"2 4 6 8 10 12 14\" is there a mistake?","Yes","No","1","0"},
            {"Are you stupid?","Yes","No","0","0"}};
    private String[] MAP = MAP_ORIGINAL;
    private int height;
    private int width;
    private boolean [][] hasStars;
    public static String tmp;
    private int [] WALL = {
            '1','2','3',
            '4','6','7',
            '8','9'};
    private Random random;
    
    public Map() {
        height = MAP.length;
        width = MAP[0].length();
        initDotData();
        randomMap();
    }
    
    public static String getQA(int q) {
       tmp = QUESTION[World.getStage()-1][q];
       return tmp;
    }
    
    public void userRandomMap(int x) {
        if (QUESTION[x][4] == "0") {
            QUESTION[x][4] = "1";
            randomMap();
        }
        else {}
    }
    
    public void randomMap() {
        resetMap();
        random = new Random();
        char item;
        for (int i=0; i<50; i++) {
            item = '*';
            int x = random.nextInt(17) + 1;
            int y = random.nextInt(10) + 4;
            addItemToMap(x, y, item);
        }
        for (int i=0; i<5; i++) {
            item = '.';
            int x = random.nextInt(17) + 1;
            int y = random.nextInt(7) + 6;
            addItemToMap(x, y, item);
        }
    }
    
    private void resetMap() {
        for (int i=4;i<15;i++) {
            for (int j=1;j<18;j++) {
                addItemToMap(j, i, '#');
            }
        }
    }
    
    private void addItemToMap(int x, int y, char item) {
        char[] tmp = MAP[y].toCharArray();
        if (tmp[x] != '0') {
            tmp[x] = item;
            MAP[y] = String.valueOf(tmp);
        }
    }
    
    public int getHeight() {
        return height;
    }
 
    public int getWidth() {
        return width;
    }
    
    public boolean hasGroundAt(int r, int c) {
        return MAP[r].charAt(c) == '#';
    }
    
    public boolean hasBushAt(int r, int c) {
        return MAP[r].charAt(c) == '*';
    }
    
    public boolean hasStarAt(int r, int c) {
        return MAP[r].charAt(c) == '.';
    }
    
    public boolean hasAnsAt(int r, int c) {
        return MAP[r].charAt(c) == '0';
    }
    
    public int hasWallAt(int r,int c) {
        for (int i=0;i<8;i++) {
            if (MAP[r].charAt(c) == WALL[i])
                return WALL[i];
        }
        return 0;
    }
    
    private void initDotData() {
        hasStars = new boolean[height][width];
        for (int r = 0; r < height; r++) {
            for(int c = 0; c < width; c++) {
                hasStars[r][c] = MAP[r].charAt(c) == '.';
            }
        }
    }
    
    public void removeStarAt(int r, int c) {
        hasStars[r][c] = false;
        char[] tmp = MAP[r].toCharArray();
        tmp[c] = '#';
        MAP[r] = String.valueOf(tmp);
    }
    
}
